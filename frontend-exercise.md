When writing code do not worry about the browser inconsistencies or design (you can target a single browser you feel comfortable with).  
  

*Also, write the code the way you will write it in production. Some guidelines are:*  
1. Try avoiding using random names for variables/functions. The naming of methods should be relevant to the code domain.  
2. Please check for small things like syntax errors, since details matter.  
**3. Focus on Primary points first, without primary points optional doesn't countable.  All the optional is just for extra points and not mandatory.**

---

## Test Details

**1. Create a small “React with webpack/npm” file structure.**  
[Optional] Extra bonus point for each below point :-  
-- Server Side Rendering (SSR)  
-- SCSS Preprocessor  
-- Build Minification  


**2. Create “React” working structure of below design**  
![front-end quiz](https://bitbucket.org/toasterco/quiz/raw/fac40bab3c4221f0d8e132e22f1f8735eae5c4f8/frontend.jpg)  

[Optional] Extra bonus point for each below point :-  
-- Clean component structure  
-- Clean semantic code.  
-- Meaningful class, variable & function names.  

**3. Functionality Details**  
3.1. TextField: Add new ‘friend’ in the list with the <Name> typed in input on ENTER.  
3.2. Favourite & Delete button functionality.  
3.3. Add pagination support to the list when there are more than 2 entries.  
3.4. Add option to select sex of a friend male/female and display it.  


[Optional] Extra Bonus Points :-  
-- Add tests using your preferred testing tool (jest, mocha, should.js ...).  



---

## Objective

1. Please deliver something that works, non working project is an automatic disqualification.  
2. Share the code on GitHub/BitBucket.  
3. **[Bonus Point]** Share the working link on firebase/gCloud.  